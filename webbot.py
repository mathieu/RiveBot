#!/usr/bin/python2.7
#-*- coding: utf-8 -*-

from rivescript import RiveScript
from twisted.web import server, resource
from twisted.internet import reactor, endpoints
import HTMLParser

class Counter(resource.Resource):
    isLeaf = True

#    def render_GET(self, request):
#        request.setHeader("content-type", "text/plain")
#        print request
#        return "hello\n"
    def render_POST(self, request):
        #print request.args["msg"]
        message = u''.join(h.unescape(request.args["msg"]))
        answer = rs.reply("localuser", message)
        # unicodedata.normalize('NFKD', answer).encode('ascii','ignore')
        return answer.encode('ascii', 'xmlcharrefreplace')


rs = RiveScript()
rs.load_directory("./brain")
rs.sort_replies()
h = HTMLParser.HTMLParser()

endpoints.serverFromString(reactor, "tcp:1234").listen(server.Site(Counter()))
reactor.run()
