#!/usr/bin/python2.7
#-*- coding: utf-8 -*-

from rivescript import RiveScript

rs = RiveScript()
rs.load_directory("./brain")
rs.sort_replies()

while True:
    msg = raw_input('>');
    if msg == '/quit':
        quit()
    reply = rs.reply("localuser", msg);
    print(u'Bot > {0}'.format(reply))
