#!/usr/bin/python2.7
#-*- coding: utf-8 -*-

from rivescript import RiveScript
from twisted.internet import protocol, reactor, endpoints
from twisted.protocols.basic import LineReceiver

class Echo(LineReceiver):
    def lineReceived(self, line):
        print("I just receive "+line)
        self.sendLine('{0}'.format(rs.reply("localuser",line)))

class EchoFactory(protocol.Factory):
    def buildProtocol(self, addr):
        return Echo()


rs = RiveScript()
rs.load_directory("./brain")
rs.sort_replies()

endpoints.serverFromString(reactor, "tcp:1234").listen(EchoFactory())
reactor.run()
